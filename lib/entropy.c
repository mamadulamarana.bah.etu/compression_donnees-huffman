#include "entropy.h"
#include <stdio.h>

void count_occurrences(FILE *file, int counts[]) {
    int c;
    while ( (c = fgetc(file)) != EOF) {
        counts[c] = counts[c] + 1;
        }
}

struct file_stat entropy(int counts[]) {
    struct file_stat entrop;
	int i = 0;
	int n = 0;
	float sum = 0.0;
	while ( i < 256){
		if (counts[i]){
		sum = (sum + counts[i]*log2(counts[i]));
		n = n + counts[i];
		}
		i = i + 1;
	}
	float entropie = log2(n) - (sum / n);
	entrop.entropy = entropie;
	entrop.size = n;
	return entrop;

}
