### Ce dépôt correspond au tp huffman.

## BAH Mamadu Lamarana

## Avancées du tp:

*toutes les fonctions du tp s'éxecute normalement.
 Vous pouvez tester les fonction :
 
 create_huffman_foresten exécutant la commande ```make```  puis ```./main tests/test1```

 build_huffman_tree
  en exécutant la commande ```make```  puis ```./main tests/test1``` ou ```./main tests/test3```

 huffman_coding
   en exécutant la commande ```make```  puis  ``` ./huffman -c tests/test1 tests/test1.huf ```
   puis visualiser le fichier via un éditeur en ligne vérifier https://en.webhex.net/

 huffman_decoding  et huffman_decode_file
   en exécutant la commande ```make```  puis  ``` ./huffman -d tests/test1.huf tests/test.d ```

### Adresses des files
image => https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/2018_-_Nyhavn_on_sunset.jpg/750px-2018_-_Nyhavn_on_sunset.jpg

audio => https://upload.wikimedia.org/wikipedia/commons/a/a9/A_Carol_Brynging_in_the_Bore%27s_Heed.ogg

code Source => 
https://gitlab.com/mica-automated-pumping-system/automated-temperature-monitoring/-/raw/main/alarm-framework/ADCService.h?inline=false

fichier Zip => https://gitlab.com/jkushmaul/rust_msvc/-/archive/master/rust_msvc-master.zip?path=src

### Expériences
la taille des fichiers compréssés est superieure à la taille des fichiers avec un codage optimal ce qui est normal
car l'entropy est presque inatteignable mais pour les fichiers mp3, on dirait ils sont déja codé avec un codage optimal.
